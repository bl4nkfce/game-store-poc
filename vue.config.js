module.exports = {
  css: {
    loaderOptions: {
      sass: {
        prependData: '@import "@/styles/_variables.scss";',
      },
    },
  },
  pluginOptions: {
    testAttrs: {
      attrs: ['test-id'],
    },
  },
};
