export default {
  productTile: '[data-test-id="productTile"]',
  productDiscountBadge: '[data-test-id="productDiscountBadge"]',
  productAddToCartButton: '[data-test-id="productAddToCartButton"]',
};
