import selectors from './config';

describe('Product', () => {
  before(() => {
    cy.visit('http://localhost:8080/');
  });

  it('Renders the product tile', () => {
    cy.get(selectors.productTile).should('be.visible');
  });
});
