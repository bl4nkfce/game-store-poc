export default {
  cartContainer: '[data-test-id="cartContainer"]',
  cartButton: '[data-test-id="cartButton"]',
  cartDropdown: '[data-test-id="cartDropdown"]',
  cartDropdownHeading: '[data-test-id="cartDropdownHeading"]',
  cartTotalValue: '[data-test-id="cartTotalValue"]',
  cartClearButton: '[data-test-id="cartClearButton"]',
  cartItems: '[data-test-id="cartItems"]',
  cartItemCount: '[data-test-id="cartItemCount"]',
  cartLocalStorageName: 'cart',
};
