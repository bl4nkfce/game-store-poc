import selectors from './config';

describe('Cart', () => {
  before(() => {
    cy.visit('http://localhost:8080/');
    cy.clearLocalStorage(selectors.cartLocalStorageName);
  });

  it('does not render cart dropdown initially', () => {
    cy.get(selectors.cartContainer)
      .find(selectors.cartDropdown)
      .should('not.be.visible');
  });

  it('toggles cart dropdown on click', () => {
    cy.get(selectors.cartContainer)
      .find(selectors.cartDropdown)
      .should('not.be.visible');

    cy.get(selectors.cartButton).click();

    cy.get(selectors.cartContainer)
      .find(selectors.cartDropdown)
      .should('be.visible');

    cy.get(selectors.cartButton).click();

    cy.get(selectors.cartContainer)
      .find(selectors.cartDropdown)
      .should('not.be.visible');
  });
});
