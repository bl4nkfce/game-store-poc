import selectors from './config';

describe('Game of the week', () => {
  before(() => {
    cy.visit('http://localhost:8080/');
  });

  it('Renders the secret button', () => {
    const secretButton = cy.get(selectors.secretButton);
    secretButton.should('be.visible');
  });

  it('Renders the Game Of The Week image', () => {
    const gotwImage = cy.get(selectors.gameOfTheWeekImage);
    gotwImage.should('be.visible');
  });
});
