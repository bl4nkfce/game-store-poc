export default {
  secretButton: '[data-test-id="secretButton"]',
  gameOfTheWeekImage: '[data-test-id="gameOfTheWeekImage"]',
};
