/* eslint-disable import/prefer-default-export */
import { Product } from './productService';

export class CartService {
  private static instance: CartService;

  static getInstance(): CartService {
    if (!CartService.instance) {
      CartService.instance = new CartService();
    }
    return CartService.instance;
  }

  public getCartItems() {
    try {
      const cart = localStorage.getItem('cart');
      return cart ? JSON.parse(cart) : [];
    } catch (error) {
      console.error('Unable to read the cart', error);
      return [];
    }
  }

  public saveCart(cart: Product[]) {
    try {
      localStorage.setItem('cart', JSON.stringify(cart));
    } catch (error) {
      console.error('Unable to read the cart', error);
    }
    return cart;
  }
}
