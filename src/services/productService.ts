import axios, { AxiosInstance } from 'axios';

export interface ProductPrice {
  baseValue: string;
  discountValue: number;
  value: string;
  isDiscounted: boolean;
}

export interface Product {
  title: string;
  price: ProductPrice;
  thumbnail: string;
  discount: number;
  isOwned: boolean;
  id: number;
}

export class ProductService {
  private static instance: ProductService;

  private http: AxiosInstance;

  private constructor() {
    this.http = axios.create();
  }

  static getInstance(): ProductService {
    if (!ProductService.instance) {
      ProductService.instance = new ProductService();
    }
    return ProductService.instance;
  }

  public getProducts(): Promise<Product[]> {
    return this.http.get<Product[]>('/products.json').then((res) => res.data);
  }

  public getGameOfTheWeek(): Promise<Product> {
    return this.http.get<Product>('/gotw.json').then((res) => res.data);
  }
}
