import { Product, CartService } from '@/services';
import { Module, VuexModule, Mutation } from 'vuex-module-decorators';

const cartService = CartService.getInstance();

export enum CartMutations {
  AddItem = 'ADD_ITEM',
  RemoveItem = 'REMOVE_ITEM',
  ClearCart = 'CLEAR_CART',
}

export enum CartGetters {
  CartItems = 'CART_ITEMS',
  TotalValue = 'TOTAL_VALUE',
  ItemCount = 'ITEM_COUNT',
  IsInCart = 'IS_IN_CART',
}

@Module
export default class CartModule extends VuexModule {
  cartItems: Product[] = cartService.getCartItems();

  @Mutation
  [CartMutations.AddItem](item: Product) {
    const isInCart = this.cartItems.find((cartItem) => cartItem.id === item.id);
    if (isInCart) {
      return;
    }
    this.cartItems = [...this.cartItems, item];
    cartService.saveCart(this.cartItems);
  }

  @Mutation
  [CartMutations.RemoveItem](id: number) {
    this.cartItems = this.cartItems.filter((cartItem) => cartItem.id !== id);
    cartService.saveCart(this.cartItems);
  }

  @Mutation
  [CartMutations.ClearCart]() {
    this.cartItems = [];
    cartService.saveCart(this.cartItems);
  }

  get [CartGetters.CartItems]() {
    return this.cartItems;
  }

  get [CartGetters.TotalValue]() {
    return this.cartItems.reduce<number>(
      (total, cartItem) => total + parseFloat(cartItem.price.value),
      0,
    );
  }

  get [CartGetters.ItemCount]() {
    return this.cartItems.length;
  }

  get [CartGetters.IsInCart]() {
    return (id: number) => this.cartItems.find((item) => item.id === id);
  }
}
