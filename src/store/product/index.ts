import { Product, ProductService } from '@/services';
import { Module, VuexModule, MutationAction } from 'vuex-module-decorators';

const productService = ProductService.getInstance();

export enum ProductMutations {
  SetProducts = 'SET_PRODUCTS',
  SetGameOfTheWeek = 'SET_GAME_OF_THE_WEEK',
}

export enum ProductGetters {
  Products = 'PRODUCTS',
  GameOfTheWeek = 'GAME_OF_THE_WEEK',
}

export enum ProductActions {
  FetchProducts = 'FETCH_PRODUCTS',
  FetchGameOfTheWeek = 'FETCH_GAME_OF_THE_WEEK',
}

@Module
export default class ProductModule extends VuexModule {
  products: Product[] = [];

  gameOfTheWeek: Product | null = null;

  @MutationAction({ mutate: ['products'] })
  async [ProductActions.FetchProducts]() {
    const products = await productService.getProducts();
    return { products };
  }

  @MutationAction({ mutate: ['gameOfTheWeek'] })
  async [ProductActions.FetchGameOfTheWeek]() {
    const gameOfTheWeek = await productService.getGameOfTheWeek();
    return { gameOfTheWeek };
  }

  get [ProductGetters.Products]() {
    return this.products;
  }

  get [ProductGetters.GameOfTheWeek]() {
    return this.gameOfTheWeek;
  }
}
