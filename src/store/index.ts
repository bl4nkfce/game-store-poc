import Vue from 'vue';
import Vuex from 'vuex';
import ProductModule from './product';
import CartModule from './cart';

Vue.use(Vuex);
export default new Vuex.Store({
  modules: {
    products: ProductModule,
    cart: CartModule,
  },
});
